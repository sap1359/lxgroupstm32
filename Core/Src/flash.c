/**
  ******************************************************************************
  * @file           : flash.c
  * @brief          : Handling the internal flash
  ******************************************************************************
  * @important		
	*	1.	This file handle the internal flash using an index which is updated during
	*			bootup process, to point to the next free location in the Flash memory.
	*			There is a pre-defined boundry in flash_define.h, to limit the area to store samples.
	*			Fore more information about the pattern of the stored data please refer to 
	*			flash_define.h.
	*					
	******************************************************************************
  */

#include "main.h"
#include "stdint.h"
#include "string.h"
#include "flash_define.h"

/* The write index which is adjusted at the boot time */
static uint32_t writeIndex;
static uint32_t readIndex;
/*-----*/


/**
  * @brief  		This function return a sample data from the Flash memory.
	* @param[out]	Argument1: The actual retrived data from Flash.
	* @param[in]	Argument2: The atsrt address of data.
  * @retval 		None.
  */
static void getSampleData(uint8_t* data, uint32_t address)
{
	int ctr;
	uint8_t* ptr;
	
	ptr = ((uint8_t*)address);
	
	for(ctr=0; ctr<FLAS_FRM_TOT_LEN; ctr++)										/* Copy data from >>address<< to the >>data<< */
		{
			data[ctr] = *(ptr + ctr);
		}
}
	

/**
  * @brief  		This function check that the data is programmed or not.
	* @param[in]	Retrived data from the flash. 
	* @retval 		0=programmed, 1=contains data
  */
static int isDataPatternUnprogrammed(uint8_t* data)
{
	int ctr;
	int retval = 1;
	
	for(ctr=0; ctr<FLAS_FRM_TOT_LEN; ctr++)
	{
		if (data[ctr] != UNPROGRAMED_DATA)											/* Check if the next read data field is empty (i.e. there is no other data on Flash) */
		{
			retval = 0;
		}
	}
	return retval;
}


/**
  * @brief  		This function check that the data is valid or not.
	* @attention	Data pattern is illustrated in detail in flsh_define.h.	
	* @param[in]	Retrived data from the flash. 
	* @retval 		0=invalid, 1=valid pattern
  */
static int isDataPatternValid(uint8_t* data)
{
	int retval = 1;
	
	if (data[FLSH_STRT_OFS] != FLSH_DATA_STATRT)							/* Check for pre-defined data structure on Flash (refer to flash_define.h */
		retval = 0;
	
	if (data[FLSH_END_OFS] != FLSH_DATA_END)									/* Check for pre-defined data structure on Flash (refer to flash_define.h */
		retval = 0;
	
	return retval;
}


/**
  * @brief  		This function check that the data is sent bu UART,previously, or not.
	* @attention	Data pattern is illustrated in detail in flsh_define.h.	
	*							When data is sent with UART interface, it is checked as read, 
	*							and willn't be sent at the next time, even in the case of restarting system. <<< Very good feature ;)
	* @param[in]	Retrived data from the flash. 
	* @retval 		0=not read, 1=is read
  */
static int isDataRead(uint8_t* data)
{
	int retval = 0;
	
	if (data[FLSH_STUS_OFS] == FLSH_DATA_READ)								/* Check that data was sent to the PC or not (i.e. is there any new data to send?) */
		retval = 1;
	
	return retval;
}


/**
  * @brief  		This function return the next valid location in the Flash.
	* @attention	In the rare case of power-outage during write in Flash memory
	*							sometimes data may be written, incompletely. This function adjust 
	*							the pointer to the next valid location.
	* @param[in]	The start address of searching for valid pattern.
	* @retval 		The address of the next valid pattern.
  */
static uint32_t getNextValidPattern(uint32_t address)
{
	uint8_t* ptr;
	
	for(; address<=FLASH_END_ADDR; address++)
	{
		ptr = ((uint8_t*)address);

		if ((*ptr == UNPROGRAMED_DATA) || (*ptr == FLSH_DATA_READ))				/* Serch for the first location of the next sample (0XFF/0X88). Fore more info. refer to the flash_define.h. */
		{
			break;
		}
	}
	return address;
}


/**
  * @brief  		This function updates the >>writeIndex<< variable at the start-up
	* @param  		None.
  * @retval 		None.
  */
static uint32_t getWriteIndex(void)
{
	uint32_t address;
	uint32_t index;
	uint8_t* ptr;
	
	index = FLASH_STR_ADDR;
	
	for(address=FLASH_STR_ADDR; address<=FLASH_END_ADDR; address++)			/* Search all the Flash storage space related to store samples to find free location. */		
	{
		ptr = (uint8_t*) address;
		
		if ((*ptr) != UNPROGRAMED_DATA)
		{
			index = address + 1;																						/* Assign the first free location to >>writeIndex<< */
		}
	}
	
	return index;	
}


/**
  * @brief  		This function updates the >>readIndex<< variable at the start-up
	* @param  		None.
  * @retval 		None.
  */
static uint32_t getreadIndex(void)
{
	uint32_t address;
	uint8_t buffer[FLAS_FRM_TOT_LEN];
	
	for(address=FLASH_STR_ADDR; address<=FLASH_END_ADDR; address+=FLAS_FRM_TOT_LEN)
	{
		getSampleData(buffer, address);
		
		if (isDataPatternUnprogrammed(buffer) == 1)				/* Flash storage is empty */
		{
			break;										
		}
		
		if (isDataPatternValid(buffer) == 1)
		{
			if (isDataRead(buffer) == 0)										/* The start address of the first unread stored sample. */
			{
				break;
			}
		}
		else
		{
			address = getNextValidPattern(address);					/* Skip from bad written samples (resolving the power-outage problem during write) */
		}
	}
	
	return address;	
}

/**
  * @brief  		This function check whether flash is full or not.
	* @param  		None.
  * @retval 		0=No, 1=Yes
  */
static int isFlashFull(void)
{
	int state = 0;
	
	if (writeIndex >= FLASH_END_ADDR)					/* Flash is full */
	{
		state = 1;
	}
	
	return state;
}


/**
  * @brief  		This function checks the stored data status as read.
	* @attention	The status field (i.e. the first byte) of the stored 
	*							data in changed fro 0Xff to 0X88. For more refer to flash_defines.h.
	* @important	After the change, stored data willn't be sent to the PC in the 
	*							responce of SEND_SAMP command, in the next time. Just new 
	*							collected data will be send. 
	* @param[in]	Sample's data.
  * @retval 		None.
  */
void checkStoredDataAsRead(uint32_t dataStartAddress)
{
	int retry;
	HAL_StatusTypeDef status;
	
	HAL_FLASH_Unlock();																				/* Unlock the protection mechanism of Flash */
	
	do
	{
		/* Change the read state of the sored sample data. */
		status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,dataStartAddress, FLSH_DATA_READ);
				
		retry++;
				
		HAL_Delay(5);																						/* A short delay in case of any error */
				
	} while ((status != HAL_OK) && (retry < MAX_RETRY));			/* A retry mechanism is used to prevent infinit loop for currupted Flash */
	
	HAL_FLASH_Lock();																					/* Lock the protection mechanism of Flash */
}


/**
  * @brief  		This function Initiate the Flash variables.
	* @attention	The >>readIndex<< and >>writeIndex<< are initialized during start-up
	*							to have a copy of indexes on the RAm in order to reduce access to 
	*							the Flash memory (Flash if considerably slower than RAM ;) 
	* @param  		None.
  * @retval 		None.
  */
void initialFlash(void)
{
	writeIndex = getWriteIndex();
	readIndex = getreadIndex();
}


/**
  * @brief  		This function Write a data sample into the flash memory.
	* @attention	4 Byte are written to the flash as the input data.
	* @param[in]	Sample's data to write to the flash.
  * @retval 		OK=HAL_OK, Flash is full=HAL_ERROR
  */
HAL_StatusTypeDef writeSample(uint8_t* data)
{
	int ctr, retry;
	int flash_buffer[FLAS_FRM_TOT_LEN];
	HAL_StatusTypeDef status = HAL_OK;
	
	if (isFlashFull() == 0)																			/* Check if flash is full or not */
	{
		HAL_FLASH_Unlock();																				/* Unlock the protection mechanism of Flash */
		
		flash_buffer[FLSH_STUS_OFS] = UNPROGRAMED_DATA;						/* Read status remain unprogrammed until retriving data from UART */
		flash_buffer[FLSH_STRT_OFS] = FLSH_DATA_STATRT;						/* Sample data structure (refer to flash_define.h) */
		
		for(ctr=0; ctr<FLAS_FRM_DAT_LEN; ctr++)
		{
			flash_buffer[ctr + FLSH_DATA_OFS] = data[ctr];
		}
		
		flash_buffer[FLSH_END_OFS] = FLSH_DATA_END;								/* Sample data structure (refer to flash_define.h) */
		
		for(ctr=0; ctr<FLAS_FRM_TOT_LEN; ctr++)										/* Write all 7 bytes of sample data */
		{
			retry = 0;
			do
			{
				status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE,writeIndex, flash_buffer[ctr]);
				
				retry++;
				
				HAL_Delay(5);																					/* A short delay in case of any error */
				
			} while ((status != HAL_OK) && (retry < MAX_RETRY));		/* A retry mechanism is used to prevent infinit loop for currupted Flash */

			if (retry > MAX_RETRY)																	/* Don't update >>writeIndex<< in the case of corrupted flash	*/																
			{
				break;
			}		
			
			writeIndex++;																						/* Update the >>writeIndex<< */
		}
			
		HAL_FLASH_Lock();																					/* Lock the protection mechanism of Flash */
	}
	else
	{
		status = HAL_ERROR;
	}
	
	return status;
}


/**
  * @brief  		This function return maimum 10 sample's data each time.
	* @attention	When the sample data are read from flash memory their status 
	*							in the flash memory will be changed to the read from unread state.
	* @param[out]	Sample's data.
  * @retval 		The real number of data in byte.
  */
uint32_t retriveUnreadData(uint8_t* data)
{
	int ctr = 0;
	int i;
	
	uint8_t buffer_1samp[FLAS_FRM_TOT_LEN];
	uint8_t	buffer_10samps[FLAS_FRM_DAT_LEN * 10];
	
	memset(data, 0, 40);
	memset(buffer_1samp, 0, 7);
	memset(buffer_10samps, 0, 40);
	
	while((readIndex < FLASH_END_ADDR) && (ctr < 10))							/* Maximum 10 samples are collected each time */
	{
		getSampleData(buffer_1samp, readIndex);														/* Get sample data */

		if (isDataPatternUnprogrammed(buffer_1samp) == 1)									/* Ther is no more stored data to retreive */
		{
			break;																		
		}
		
		if ((isDataPatternValid(buffer_1samp) == 1) && (isDataRead(buffer_1samp) == 0))											/* If data are stored completely, and isn't read previously, retrievr it */												
		{
			memcpy(buffer_10samps + (ctr * FLAS_FRM_DAT_LEN), buffer_1samp + FLSH_DATA_OFS, FLAS_FRM_DAT_LEN);		/* Copy to the input argument, outside this function */
			
			checkStoredDataAsRead(readIndex);													/* Check the retrieved data as read >>>> It can be also to do this task upon receive ACK from UART, however, it will be so more complicated to implement :) <<<< */
			
			readIndex += FLAS_FRM_TOT_LEN;														/* Adjust the >>readIndex for the next time */
			
			ctr++;																										/* Maximum 10 samples ;) */
		}
		else
		{
				readIndex = getNextValidPattern(readIndex);							/* In the case of any error related to the power-outage -during write- go to the next valis sample */
		}
	}
	
	for(i=0; i<(ctr * FLAS_FRM_DAT_LEN); i++)											/* Fill the input argument */
	{
		data[i] = buffer_10samps[i];
	}
	
	return ctr * FLAS_FRM_DAT_LEN;																/* Return the total bytes corresponding to retrieved data from Flash memory */
}


/**
  * @brief  		This function erase the storage area of the flash.
	* @param			None.
  * @retval 		None.
  */
void eraseStorageAre(void)
{
	HAL_FLASH_Unlock();																				/* Unlock the protection mechanism of Flash */

	FLASH_Erase_Sector(FLASH_ARE_SECTOR, VOLTAGE_RANGE_3);		/* Erase the entire sector */

	HAL_FLASH_Unlock();																				/* Unlock the protection mechanism of Flash */
}
