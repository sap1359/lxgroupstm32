/**
  ******************************************************************************
  * @file           : uart.c
  * @brief          : Handling UART & serial communication
  ******************************************************************************
  * @attention		
	*	1.	For more flexible code that can be used as an infrastructure in other projects, 
	*			I decide to use DMA style, because in this way, you can change the command 
	*			(,and consequently command lenght) without any limitation which would be 
	*			forced by using simple blocking Transmit/Receive functions.	
	*
	* 2.	Two variables (RXLength & lastRXLength) are used to implement an state machine.
	*			It is the best way to control RX from uart because of the fact that 
	*			it is high-performance, easy to implement, and very flexible. ;)
	*
	*	3.	This file contains a critical type of two writers at the same time.
	*			The source of race condition is the extern variable >>mode<<.
	*			The other parts of the critical section are implemented in main.c & button.c.
	*			This critical section is controlled by >>mutex_mode_id<<.
	*	
	*	4.	The send-buffer in the UART module is also another source of the race condition
	*			which is controlled by the >>mutexSendID<<.
	*
	******************************************************************************
  */
	
#include "main.h"
#include "cmsis_os.h"
#include "stdlib.h"
#include "string.h"
#include "flash.h"
#include "packet.h"
#include "datatype.h"
#include "uart_define.h"
#include "packet_define.h"
#include "system_define.h"

extern UART_HandleTypeDef huart5;
extern DMA_HandleTypeDef hdma_uart5_rx;
/*-----*/

void uartReceiveDetectTask(void const * argument);
void uartTransmitTask(void const * argument);

osThreadId uartRxTaskHandle;
osThreadId uartTxTaskHandle;
/*-----*/

osMutexDef (MutexSend);				// Mutex name definition to control UART send
osMutexId mutexSendID;       
/*-----*/

/*	Save the received bytes in each calling of the DMA Int. 
*		It is defined as array to facilitate changing in the future, if it is needed. */
static uint8_t uartDMAInternalBuffer[UART_MAX_DMA_LEN];			

/*	The main uart buffers */
static uint8_t uartRXBuffer[UART_RX_LEN];
static uint8_t uartTXBuffer[UART_TX_LEN];

/* 	State-machine variables to detect the end of the received packet */
static uint8_t RXLength = 0;
static uint8_t lastRXLength = 0;

/**
	*		you can simply fill the transfer buffer and tx_len, packet automatically 
	*		is sent in the transmit task. 
	*/
static uint8_t TXLength = 0;
/*-----*/


/**
  * @brief  		This function clean the send buffer. 
	* @attention	This function should be called before sending a new packet.
  * @param  		None.
  * @retval 		None.
  */
static void clearSendBuffer(void)
{
	memset(uartTXBuffer, 0, UART_TX_LEN);
	
	TXLength = 0;
}


/**
  * @brief  		This function clean the receive buffer. 
	* @attention	This function should be called before receiving a new packet.  
	* @param  		None.
  * @retval 		None.
  */
static void clearReceiveBuffer(void)
{
	memset(uartRXBuffer, 0, UART_RX_LEN);
	
	RXLength = 0;
}


/**
  * @brief  		This function initiate the DMA on UART. 
	* @attention	This is declared to keep the uartDMAInternalBuffer variable 
	*							as an static variable.
  * @param  		None.
  * @retval 		None.
  */
static void uartInitInterrupt(void)
{
	/* Enable DMA for the first byte */
	HAL_UART_Receive_DMA(&huart5, uartDMAInternalBuffer, UART_DMA_LEN);
}


/**
  * @brief  		This function creates RX & TX tasks
	* @attention	RX task checks the receive complete using DMA interrups
	*							TX task use simple blocking transmit
  * @param  		None.
  * @retval 		None.
  */
static void uartCreateTxRxTasks(void)
{
	osThreadDef(uartRxTask, uartReceiveDetectTask, osPriorityHigh, 1, UARTRX_TASK_STK_SIZ);
	uartRxTaskHandle = osThreadCreate(osThread(uartRxTask), NULL);

	
	osThreadDef(uartTxTask, uartTransmitTask, osPriorityHigh, 1, UARTTX_TASK_STK_SIZ);
	uartTxTaskHandle = osThreadCreate(osThread(uartTxTask), NULL);
}


/**
	* @brief  		This function create the required Mutexes.
	* @param  		None.
  * @retval 		None.
  */
static void createMutexes(void)
{
	mutexSendID = osMutexCreate(osMutex(MutexSend));
}


/**
  * @brief  		This function initialize the UART
  * @param  		None.
  * @retval 		None.
  */
void initialUART()
{
	clearReceiveBuffer();
	clearSendBuffer();
	
	uartInitInterrupt();
	createMutexes();
	uartCreateTxRxTasks();
}


/**
  * @brief  		This is called upon receive-complete event for uart.
	* @attention	This function uses the receive buffer (i.e. uartRXBuffer).
	*							This function fill the uartTXBuffer & TXLength, only.
	*							Packet will be transmit in the uartTransmitTask. 
	*	@important	This function is a part of critical section due to using 
	*							global variable >>mode<<. Another part is located in button.c
  * @param  		None
  * @retval 		None
  */
static void parse_receive_command(void)
{
	enum uartCommand com;
	
	com = getCommand((uint8_t*)uartRXBuffer, RXLength);
	
	switch (com)				/* parse receive packet */
	{
		case send:
		{
			if (getMode() == idle)														/* Don't retrieve data in sampling, because it leads to another critical section in Flash*/
			{
				TXLength = retriveUnreadData(uartTXBuffer);			/* Retrive data from flash and send to PC - maximum of 10 data samples (i.e. 40 Bytes) will be sent each time */
			
				if (TXLength == 0)
				{
					TXLength = createNODATAPackage(uartTXBuffer);	/* If there is no unread dataon flash (i.e. new data) send a pre-defined message */
				}
			}
			else 
			{
				TXLength = createNACKPackage(uartTXBuffer);			/* In sampling mode reply Busy */
			}
		}
		break;
		
		case start:
		{
			TXLength = createACKPackage(uartTXBuffer);			/* Send ACK messag to inform about start sampling */
			setMode(sampling);															/* >>> Critical section <<< - mode is the source of race condition */
		}
		break;
		
		case stop:
		{
			TXLength = createACKPackage(uartTXBuffer);			/* Send ACK messag to inform about stop sampling */
			setMode(idle);																	/* >>> Critical section <<< - mode is the source of race condition */
		}
			break;
		
		case erase:
		{
			if (getMode() == idle)													/* using this conditional statement it is no require to handle the flash area using critical section mechanisn */
			{		
				eraseStorageAre();														/* Erase the flash area */
				
				TXLength = createACKPackage(uartTXBuffer);		/* Send ACK messag to inform about erase flash */
			}
			else
			{
				TXLength = createNACKPackage(uartTXBuffer);		/* return pre-defined error message - device is busy*/
			}
		}
			break;
		
		case error:
		default:
		{
			TXLength = createNACKPackage(uartTXBuffer);			/* return pre-defined error message */
		}
			break;
	}
}


/**
  * @brief  		Function implementing the task of receive in UART. 
	* @attention	This function detects the end of the received packet and 
	*							call the parser function to do appropriate action.
	*							This function check the received data every 1ms.
  * @param[in]	argument: Not used.
  * @retval 		None.
  */
void uartReceiveDetectTask(void const * argument)
{
  for(;;)
  {
		/* Implementing the state-machine - start*/
		if (lastRXLength != RXLength)
		{
			lastRXLength++;
		}
		else if ((lastRXLength == RXLength) && (RXLength > 0))		/* ** Receive is complete** */
		{		
			HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);	
			parse_receive_command();													/* Do appropriate reaction */	

			lastRXLength = 0;
	
			/* Prepare receive buffer for the next packet */			
			clearReceiveBuffer();
		}
		/* Implementing the state-machine - end*/
		
		osDelay(UART_RX_INTRVL);
  }
}


/**
  * @brief  		Function implementing the transfer task for the UART module. 
	* @attention	This function send TX buffer whenever there are data to send.
	*							You can just fill the buffer and adjust the >>TXLength<< to send 
	*							data, automatically on the UART.
  * @param[in]	argument: Not used.
  * @retval 		None.
  */
void uartTransmitTask(void const * argument)
{
  for(;;)
  {
		osDelay(SHORT_PER);

		if (TXLength > 1u)		/* if there is any thing to send */
		{
			osMutexWait(mutexSendID, osWaitForever);					/* >>> Critical section start <<< - another part of the race is located in uartSendData function, which can be called in parallen with receiving packet and need to reply */			
			
			HAL_UART_Transmit(&huart5, uartTXBuffer, TXLength, UART_TOUT);
			
			osMutexRelease(mutexSendID);											/* >>> Critical section end <<< */
		}
		
		/* It is required due to the fact that transmit is detected by TXLength (above if structure) */
		clearSendBuffer();
  }
}


/**
  * @brief  		This is the overrided function of UART-DMA
	* @attention	Don't call time-consuming jobs in this function. 
	*							It is executed in the interrupt context.
  * @param[in]	argument: The uart which causes the interrupt.
  * @retval 		None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart == &huart5)
	{
		/* Enable DMA for the next byte */
		HAL_UART_Receive_DMA(&huart5, uartDMAInternalBuffer, UART_DMA_LEN);
		
		/* Append received byte to the end of packet */ 
		uartRXBuffer[RXLength] = uartDMAInternalBuffer[0];
		
		/* Implementing the state-machine - move to the stable state*/
		RXLength++;
	}
}


/**
  * @brief  		This function fill the send buffer and initiate the >>TXLength<<, accordingly.
	* @attention	This function lock the Mutex for sending data.
	* @important	The >>uartTXBuffer<< is the source of the race condition and is handled 
	*							using the >>mutexSendID<< Mutex.
	* @param[in]	argument1: Data to be sent.
	* @param[in]	argument2: The data lenght.
  * @retval 		None.
  */
void uartSendData(uint8_t* data, int len)
{
	int ctr;
	
	if (len < UART_TX_LEN)														/* Protect the overflow of the uart send buffer */
	{
		osMutexWait(mutexSendID, osWaitForever);			/* >>> Critical section start <<< - another part of the race is located in uartReceiveDetectTask function  */			

		for(ctr=0; ctr<len; ctr++)											/* Fill data into the internal buffer of UART module */
		{
			uartTXBuffer[ctr] = data[ctr];
		}
		
		TXLength = len;																		/* Cause to send message in the uartTransmitTask function */
		
		osMutexRelease(mutexSendID);									/* >>> Critical section end <<< - each request to send will be blocked until the last packet is sent */

	}
}


/**
  * @brief  		This function sends the pre-defined message on UART to
	*							indicate Flash-Full.
	* @attention	This function is used to test, merely.
	* @param			None.
  * @retval 		None.
  */
void uartSendFullMessage(void)
{
	uint8_t buffer[10];
	int len;
	
	len = createFULLPackage(buffer);									/* Create a pre-defined response */
	
	uartSendData(buffer, len);												/* Lock the Mutex and send */
}


/**
  * @brief  		This function sends the pre-defined message on UART to 
	*							indicate sampling.
	* @attention	This function is used to test, merely.
	* @param			None.
  * @retval 		None.
  */
void uartSendSamplingMessage(void)
{
	uint8_t buffer[10];
	int len;
	
	len = createSAMPPackage(buffer);									/* Create a pre-defined response */
	
	uartSendData(buffer, len);												/* To lock the Mutex and send */
}
