/**
  ******************************************************************************
  * @file           : packet.c
  * @brief          : Implementation of packet-related functions
  ******************************************************************************
  * @attention		
	*	1.	This module (i.e. packet.c & packet.h) doesn't include any internal buffer.
	*			All the helper function uses the pointer argument to work with packets.
	*			buffers are implemented outside this module, for example in uart module.
	*
	*	2.	In the case of string packet, the are several difficulties to match the 
	*			command with a pre-defined string & its lenght. 
	*			It is originated from your request. The UART command den be handled with 
	*			binary data, perfectely. ;)
	******************************************************************************
  */

#include "stdint.h"
#include "string.h"
#include "datatype.h"
#include "packet_define.h"
/*-----*/

/**
  * @brief  		This function return the command extracted from string received data.
	* @attention	This command convert the string command into the binary which 
	*							can be processed dramatically simpler than string commands.
  * @param[in]	argument1: received packet data
	* @param[in]	argument2: received packet length
  * @retval 		received command.
  */
enum uartCommand getCommand(uint8_t* packet_data, int len)
{
	/* 	String command handle with more difficulties than binary commands.
			Corresponding string commands are defined in defines.h. */
	if (strncmp((char*)packet_data, PAC_SEND_SAMP, 9) == 0)					/* SEND_SAMP -> 	9 bytes */
	{
		return send;
	}
	else if (strncmp((char*)packet_data, PAC_START_SAMP, 10) == 0)	/* START_SAMP -> 	10 bytes */
	{
		return start;
	}
	else if (strncmp((char*)packet_data, PAC_STOP_SAMP, 9) == 0)		/* STOP_SAMP -> 	9 bytes */
	{
		return stop;
	}
	else if (strncmp((char*)packet_data, PAC_ERASE, 5) == 0)				/* ERASE -> 			5 bytes */
	{
		return erase;
	}
	else
	{
		return error;
	}
}


/**
  * @brief  		This function create a pre-defined message as the ACK responce.
	* @param[out]	Transfer buffer for TX UART.
	* @retval 		The kenght of the packet (i.e. 2 for OK).
  */
int createACKPackage(uint8_t* packet_data)
{
	strncpy((char*)packet_data, PAC_ACK, ACK_PACK_LEN); 
	
	return ACK_PACK_LEN;
}


/**
  * @brief  		This function create a pre-defined message as the NACK responce.
	* @param[out]	Transfer buffer for TX UART.
	* @retval 		The kenght of the packet (i.e. 4 for NACK)
  */
int createNACKPackage(uint8_t* packet_data)
{
	strncpy((char*)packet_data, PAC_NACK, NACK_PACK_LEN);
	
	return NACK_PACK_LEN;
}


/**
  * @brief  		This function create a pre-defined message as the SAMP responce.
	* @attention	This function maybe called from the Main module
	* @param[out] Transfer buffer for TX UART.
	* @retval 		The kenght of the packet (i.e. 4 for SAMP)
  */
int createSAMPPackage(uint8_t* packet_data)
{
	strncpy((char*)packet_data, PAC_SAMP, SAMP_PACK_LEN);
	
	return SAMP_PACK_LEN;
}


/**
  * @brief  		This function create a pre-defined message as the FULL responce.
	* @attention	This function maybe called from the Main module.
	* @param[out] Transfer buffer for TX UART.
	* @retval 		The kenght of the packet (i.e. 4 for FULL)
  */
int createFULLPackage(uint8_t* packet_data)
{
	strncpy((char*)packet_data, PAC_FULL, FULL_PACK_LEN);
	
	return FULL_PACK_LEN;
}


/**
  * @brief  		This function create a pre-defined message as the FULL responce.
	* @attention	This function maybe called from the Main module.
	* @param[out] Transfer buffer for TX UART.
	* @retval 		The kenght of the packet (i.e. 6 for NODATA)
  */
int createNODATAPackage(uint8_t* packet_data)
{
	strncpy((char*)packet_data, PAC_NODATA, NODATA_PACK_LEN);
	
	return NODATA_PACK_LEN;
}
