/**
  ******************************************************************************
  * @file           : button.c
  * @brief          : Handling the push-button
  ******************************************************************************
  * @important		
	*	1.	This file contains a critical section of two writers at the same time.
	*			The source of race condition is the extern variable >>mode<<, which is 
	*			handled with the >>mutex_mode_id<< Mutex.
	*			The other parts of the critical section are implemented in main.c 
	*			uart.c, and ads1298.c.
	*					
	******************************************************************************
  */
	
#include "main.h"
#include "datatype.h"
#include "cmsis_os.h"
#include "system_define.h"
	
void debounceTask(void const * argument);
osThreadId debounceTaskHandle;
/*-----*/


/**
 * @brief  		This function create the debouncing task for button. 
 * @param  		None.
 * @retval 		None.
 */
void initialButton(void)
{
	osThreadDef(debounceTask, debounceTask, osPriorityHigh, 1, BUTN_TASK_STAK_SIZ);
	debounceTaskHandle = osThreadCreate(osThread(debounceTask), NULL);
}
	

/**
	* @brief  		This function send signal to the running task (debounceTask).
	* @attention	This function is used to keep modularity. Using this function
	*							the ocal variable of >>debounceTaskHandle<< keep remains local (i.e. static).
	* @param  		None.
  * @retval 		None.
  */
void buttonSendSignal(void)
{
	osSignalSet(debounceTaskHandle, BTN_SIG_FLAG);				/* send a Signal to debounceTask function to activate the barrier */
}


/**
	* @brief  		Function implementing the debounce task for Button switch.
	* @attention	This function communicate with the HAL_GPIO_EXTI_Callback
	*							using signal/wait mechanism.
	*	@important	This function is a part of critical section due to using 
	*							global variable >>mode<<. Another part is located in uart.c & ads1298.c
	* @param  		argument: Not used.
  * @retval 		None.
  */
void debounceTask(void const * argument)
{
	osEvent event;
	GPIO_PinState	state;
	enum deviceMode dev_mode;
	
  for(;;)		/* task loop */
  {
		event = osSignalWait(BTN_SIG_FLAG, osWaitForever);				/* Wait for signal from HAL_GPIO_EXTI_Callback */
		osDelay(DEBOUNCE_TIME);																		/* Debouncing time-out */
		
		if (event.status == osEventSignal)												/* Validating signal */
		{
			state = HAL_GPIO_ReadPin(BUTTON_GPIO_Port, BUTTON_Pin);	/* Read the GPIO after debouncing time-out*/
			
			if (state == GPIO_PIN_RESET)														/* Check for debouncing */
			{
				dev_mode = getMode();														
				
				if (dev_mode == idle)																	/* Change the device mode upon press push-button */
				{
					setMode(sampling);																	/* >>> Critical section <<< - mode is the source of race condition */
				}
				else
				{
					setMode(idle);																			/* >>> Critical section <<< - mode is the source of race condition */
				}			
			}
		}
			/* It was better to use osSignalClear function, however it isn't implemented 
			*	 in this version of the CMSIS. 
			*	 It doesn't caused any problem by choosing appropriate debounce timeout, 
			*	 or simpler pooling mechanism can be employed. */
  }
}
