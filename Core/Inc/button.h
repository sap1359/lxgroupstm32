/**
  ******************************************************************************
  * @file           : button.h
  * @brief          : Define related functions of button
  ******************************************************************************
  * @attention
  *	-		I try to preserve all the related variables and task, as the static members. 
	*
  ******************************************************************************
  */
	
	#ifndef __BUTTON_H__
	#define __BUTTON_H__
	
	void initialButton(void);
	
	void buttonSendSignal(void);
	
	#endif
