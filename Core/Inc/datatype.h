/**
  ******************************************************************************
  * @file           : datatype.h
  * @brief          : This file includes definition of datatypes
  ******************************************************************************
  * @attention
	*	-		Datatypes should be declared in this file.
	*	
  ******************************************************************************
  */

#ifndef __DATATYPES_H__
#define __DATATYPES_H__

/*	Defining all the command on UART to convert string command into the 
*		binary values, due to have better and simpler code.*/
enum uartCommand
{
	send = 0x00,
	start,
	stop,
	erase,
	error = 0x80
};

/* 	Defines device modes. Different modes indicate the state of the device
*		using LED. Fast blinking = sampling, slow blinking = idle */
enum deviceMode
{
	sampling = 0,
	flashFull,
	idle
};




#endif
